package team.frc.morpheus.sge.api;

import team.frc.morpheus.sge.core.GameWindow;
import team.frc.morpheus.sge.core.FrameTimer;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.*;

public class Launcher {

    private void barf(Thread th, Throwable ex) throws Throwable {
        throw ex.getCause();
    }

    public static void launch(IGame game, int targetFps, boolean enableHwAccel) {
        if (enableHwAccel) System.setProperty("sun.java2d.opengl", "true");
        var frameTime = new FrameTimer();
        var gameLoop = Executors.newScheduledThreadPool(1);
        var window = new GameWindow(game, frameTime);
        window.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                gameLoop.shutdown();
                window.dispose();
                System.exit(0);
            }
        });
        Runnable timerTask = () -> {
            frameTime.start();
            game.tick(frameTime.getEstimate() * window.getWarp());
            window.drawGame();
            frameTime.end();
        };

        Runnable superSafeTimerTask = () -> EventQueue.invokeLater(timerTask);
        gameLoop.scheduleAtFixedRate(superSafeTimerTask, 0L, Math.round((1000.0/targetFps)), TimeUnit.MILLISECONDS);
    }
}
