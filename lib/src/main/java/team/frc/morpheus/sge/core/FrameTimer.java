package team.frc.morpheus.sge.core;

public class FrameTimer {
    private long now, then;
    private final long[] past = new long[10];
    private long estimate;
    private int index;
    private final double nanos = Math.pow(10, 9);

    public FrameTimer() {
        now = then = System.nanoTime();
    }

    public long get() {
        return now - then;
    }

    public long getRollingFps() {
        return (long) (nanos / estimate);
    }

    public double getEstimate() {
        return estimate / nanos;
    }

    public void start() {
        now = System.nanoTime();
    }

    public void end() {
        if (index == 0) {
            estimate = 0;
            for (long l : past) estimate += l;
            estimate /= past.length;
        }
        past[index] = get();
        index = (index + 1) % past.length;
        then = now;
    }
}

