package team.frc.morpheus.sge.api;

import java.awt.event.*;


/***
 *
 */
public interface IInputHandler extends KeyListener, MouseListener, MouseMotionListener, MouseWheelListener, ComponentListener {

    @Override default void componentResized(ComponentEvent e) {}
    @Override default void componentMoved(ComponentEvent e) {}
    @Override default void componentShown(ComponentEvent e) {}
    @Override default void componentHidden(ComponentEvent e) {}

    @Override default void keyTyped(KeyEvent e) {}
    @Override default void keyPressed(KeyEvent e) {}
    @Override default void keyReleased(KeyEvent e) {}

    @Override default void mouseClicked(MouseEvent e) {}
    @Override default void mousePressed(MouseEvent e) {}
    @Override default void mouseReleased(MouseEvent e) {}
    @Override default void mouseEntered(MouseEvent e) {}
    @Override default void mouseExited(MouseEvent e) {}
    @Override default void mouseDragged(MouseEvent e) {}
    @Override default void mouseMoved(MouseEvent e) {}
    @Override default void mouseWheelMoved(MouseWheelEvent e) {}

}
