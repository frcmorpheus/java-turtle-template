package team.frc.morpheus.sge.api;

import java.awt.*;
import java.util.Map;

/***
 * Interface describing the services you can ask the engine to provide for your SGE application. You may have
 * noticed that it doesn't start with *I*. This is because it's not an interface for you to implement - rather,
 * your application is passed an SGE-internal implementation of this interface in IGame::init.
 */
public interface Please {
    /***
     * @param name the text that will be displayed in the header bar of the window
     */
    void setTitle(String name);

    /***
     * Sets the size of the canvas. The window itself may (and probably will) be slightly larger than this size,
     * depending on the platform. SGE prevents the user from resizing the window, so it shouldn't change once you
     * set it unless something quite wacky happens.
     * @param size size of the canvas
     */
    void setSize(Dimension size);
    default void setSize(int width, int height) { setSize(new Dimension(width, height)); }

    /***
     * Allows subscribing to various AWT events - keypresses, mouse clicks, that sort of thing. You can add
     * additional handlers by calling it more than once. There's currently no way to remove them using SGE's API,
     * so don't call this in your game loop or something.
     * @param handler event listener callback
     */
    void handleInputWith(IInputHandler handler);

    /***
     * @return rolling average of FPS
     */
    long getRollingFps();

    /***
     * @param hints map/dict of rendering hints; see AWT/Java2D docs for more info
     */
    void useCustomHints(Map<RenderingHints.Key, Object> hints);

    /***
     *
     * @param factor time will appear to flow this many times faster or slower
     */
    void setWarp(double factor);
}
