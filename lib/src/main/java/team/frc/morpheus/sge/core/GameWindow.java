package team.frc.morpheus.sge.core;

import team.frc.morpheus.sge.api.IGame;
import team.frc.morpheus.sge.api.IInputHandler;
import team.frc.morpheus.sge.api.Please;
import team.frc.morpheus.sge.util.AWTHelpers;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.util.Map;
import java.util.Optional;

public class GameWindow extends Frame {
    private final IGame g;
    private BufferStrategy bs;
    private double warp = 1.0; // TODO: move this somewhere else
    private Optional<Map<RenderingHints.Key, Object>> customHints = Optional.empty();
    public GameWindow(IGame g, FrameTimer frameTime) {
        this.g = g;
        setResizable(false);
        setBackground(Color.BLACK);
        setIgnoreRepaint(true);
        Canvas canvas = new Canvas();
        canvas.setIgnoreRepaint(true);
        add(canvas);
        pack();
        addWindowFocusListener(new WindowAdapter() {
            @Override public void windowGainedFocus(WindowEvent e) { canvas.requestFocusInWindow(); }
        });
        g.init(new Please() {
            @Override public long getRollingFps() { return frameTime.getRollingFps(); }
            @Override public void setTitle(String name) { GameWindow.this.setTitle(name);}
            @Override public void handleInputWith(IInputHandler handler) { AWTHelpers.addListeners(canvas, handler); }
            @Override public void useCustomHints(Map<RenderingHints.Key, Object> hints) { customHints = Optional.of(hints); }
            @Override public void setWarp(double factor) { warp = factor; }
            @Override public void setSize(Dimension size) {
                canvas.setPreferredSize(size);
                var i = GameWindow.this.getInsets();
                size.setSize(size.width+i.left, size.height+i.top);
                GameWindow.this.setSize(size);
            }
        });
        setVisible(true);
        while ((bs = canvas.getBufferStrategy()) == null)
            canvas.createBufferStrategy(2);
    }

    public double getWarp() { return warp; }

    public void drawGame() {
        var g2d = (Graphics2D)bs.getDrawGraphics();
        g2d.setRenderingHints(AWTHelpers.defaultHints);
        customHints.ifPresent(g2d::setRenderingHints);
        g.draw(g2d);
        g2d.dispose();
        bs.show();
    }

    //@Override
    //public void paint(Graphics gBasic) {
    //    Graphics2D g2d = (Graphics2D)gBasic;
    //    g2d.translate(insets.left, insets.top);
    //    g.draw(g2d);
    //}
}
