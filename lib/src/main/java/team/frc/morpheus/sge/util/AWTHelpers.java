package team.frc.morpheus.sge.util;

import team.frc.morpheus.sge.api.IInputHandler;

import java.awt.*;
import java.util.Map;

public class AWTHelpers {
    public static void addListeners(Component c, IInputHandler h) {
        c.addComponentListener(h);
        c.addKeyListener(h);
        c.addMouseListener(h);
        c.addMouseMotionListener(h);
        c.addMouseWheelListener(h);
    }
    public static final Map<RenderingHints.Key, Object> defaultHints = Map.of(
            RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON,
            RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE
    );
}
