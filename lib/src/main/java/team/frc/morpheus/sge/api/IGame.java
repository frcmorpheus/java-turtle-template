package team.frc.morpheus.sge.api;

import java.awt.*;

/***
 * Interface you should implement in order to make an application using SGE.
 */
public interface IGame {
    void init(Please please);
    void tick(double dt);
    void draw(Graphics2D graphicsContext);
}
