package team.frc.morpheus.sge.util;

import team.frc.morpheus.sge.api.Please;

import java.awt.*;

public class DebugHelpers {
    private static final Font debugFont = new Font(Font.MONOSPACED, Font.BOLD, 18);
    public static void drawFps(Please pls, Graphics2D g) { drawFps(pls, g, 0, 0); }
    public static void drawFps(Please pls, Graphics2D g, int x, int y) {
        g.setXORMode(Color.BLACK);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        g.setFont(debugFont);
        g.drawString(String.format(" %03d FPS", pls.getRollingFps()), x, g.getFontMetrics().getAscent()+y);
        g.setRenderingHints(AWTHelpers.defaultHints);
        g.setPaintMode();
    }
}
