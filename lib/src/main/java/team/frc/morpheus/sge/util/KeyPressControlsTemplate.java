package team.frc.morpheus.sge.util;

import team.frc.morpheus.sge.api.IInputHandler;

import java.awt.event.KeyEvent;
import java.util.Map;

public abstract class KeyPressControlsTemplate implements IInputHandler {
    protected Map<Integer, Runnable> bindings;
    public KeyPressControlsTemplate() { bindings = define(); }
    public void doKeyAction(KeyEvent e) {
        int code = e.getKeyCode();
        if (bindings.containsKey(e.getKeyCode())) bindings.get(code).run();
    }
    protected abstract Map<Integer, Runnable> define();

    @Override public void keyPressed(KeyEvent e) { doKeyAction(e); }
}
