module team.frc.morpheus.sge {
    requires transitive java.desktop;
    exports team.frc.morpheus.sge.api;
    exports team.frc.morpheus.sge.util;
}