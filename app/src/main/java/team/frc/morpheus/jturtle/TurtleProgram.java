package team.frc.morpheus.jturtle;

import java.util.function.Supplier;

/**
 * Implement this interface in order to make a Turtle program.
 */
@FunctionalInterface
public interface TurtleProgram {

    /**
     * Implement this method to draw using a turtle. You have access to the turtle through the TurtleAPI object
     * passed into the method. Call its methods in order to draw; see TurtleAPI for more info.
     * @param turtle the Turtle API
     */
    void run(TurtleAPI turtle);
}
