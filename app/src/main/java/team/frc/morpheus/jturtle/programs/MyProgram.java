package team.frc.morpheus.jturtle.programs;

import team.frc.morpheus.jturtle.TurtleAPI;
import team.frc.morpheus.jturtle.TurtleProgram;

import java.awt.Color;

public class MyProgram implements TurtleProgram {

    @Override
    public void run(TurtleAPI turtle) {
        turtle.setColor(Color.RED); // use a red pen
        turtle.setPenDown(true); // put the pen on the page

        // your turtle code goes here
        // look at Hexagon.java for examples of using Turtle API to draw lines

        turtle.setPenDown(false); // pick up the pen
    }
}
