package team.frc.morpheus.jturtle;

import java.awt.*;

public interface TurtleAPI {
    /***
     * Set the color of the pen that the Turtle is holding. Look at the Javadoc for java.awt.Color for additional info.
     * Some basic colors are already defined and named for you as static fields in the Color class, e.g. "Color.RED".
     * You should be able to see these with the code completion feature of your IDE.
     * @param c the object representing the color you'd like to use
     */
    void setColor(Color c);

    /**
     * Set whether the pen is "touching the page". By default, it is not. Picking up the pen is useful when you need
     * to move the turtle to a new location without drawing a line in the process.
     * @param down
     */
    void setPenDown(boolean down);

    /**
     * Tell the turtle to go a certain number of pixels forward (in the direction of its current heading) from its
     * current position. You can change the heading using turn(). If the pen is down, a line will be drawn.
     * @param pixels
     */
    void go(int pixels);

    /**
     * Resets the turtle's position and heading. A turtle can always find its way home if it gets lost. :)
     * Before returning home, the turtle will pick up the pen, so if you want to keep drawing you will need
     * to put the pen on the page again.
     */
    void home();

    /**
     * Change the heading of the turtle. This is relative to the turtle's current heading.
     * @param degrees
     */
    void turn(double degrees);
}