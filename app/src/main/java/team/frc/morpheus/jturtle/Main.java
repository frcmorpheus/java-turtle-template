package team.frc.morpheus.jturtle;

import team.frc.morpheus.jturtle.programs.*;
import team.frc.morpheus.sge.api.Launcher;

/**
 * Most of your code will go in the programs package (for instance, in MyProgram.java), but you will need
 * to edit the selectedProgram field to run your program instead of the example program.
 */
public final class Main {

    /* Change the value of this field to run a different turtle program. Instead of "new Hexagon()", you could have
     * "new MyProgram()"
     */
    private static final TurtleProgram selectedProgram = new Hexagon();


    public static void main(String... args) {
        Launcher.launch(new Turtle(selectedProgram), 60, true);
    }
}
