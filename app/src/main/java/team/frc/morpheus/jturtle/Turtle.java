package team.frc.morpheus.jturtle;

import team.frc.morpheus.sge.api.IGame;
import team.frc.morpheus.sge.api.Please;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.util.Map;


/***
 * This class provides the TurtleAPI implementation that is used to display your turtle program on the screen.
 * You shouldn't need to modify this file unless you want to tinker with the internals.
 */
public class Turtle implements IGame {

    private TurtleProgram p;
    private boolean animate = false;
    private final BasicTurtleAPI api;

    private static class BasicTurtleAPI implements TurtleAPI {
        private Graphics2D g;
        private AffineTransform start;
        private boolean pen = false;

        void setup(Graphics2D g) {
            this.g = g;
            g.translate(400, 300);
            g.rotate(Math.PI);
            start = g.getTransform();
        }

        @Override
        public void setColor(Color c) {
            g.setColor(c);
        }

        @Override
        public void home() {
            pen = false;
            g.setTransform(start);
        }

        @Override
        public void setPenDown(boolean down) {
            pen = down;
        }

        @Override
        public void go(int pixels) {
            if (pen) {
                g.drawLine(0, 0, 0, pixels);
            }
            g.translate(0, pixels);
        }

        @Override
        public void turn(double degrees) {
            g.rotate(degrees * Math.PI / 180);
        }
    }

    public Turtle(TurtleProgram p, boolean animate) {
        this.p = p;
        this.animate = animate;
        api = new BasicTurtleAPI();
    }

    public Turtle(TurtleProgram p) {
        this.p = p;
        api = new BasicTurtleAPI();
    }

    @Override
    public void init(Please please) {
        please.setSize(800, 600);
        please.setTitle("Java Turtle");
        please.useCustomHints(Map.of(
                RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF
        ));
    }

    @Override
    public void tick(double dt) {

    }

    @Override
    public void draw(Graphics2D g) {
        var defaultTransform = g.getTransform();
        api.setup(g);
        p.run(api);
        g.setTransform(defaultTransform);
        g.setColor(Color.BLACK);
    }
}
