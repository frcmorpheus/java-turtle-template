package team.frc.morpheus.jturtle.programs;

import team.frc.morpheus.jturtle.TurtleAPI;
import team.frc.morpheus.jturtle.TurtleProgram;

import java.awt.*;

/***
 * Example Turtle program which draws a hexagon. It may be helpful to reference it when writing your own TurtleProgram.
 */
public class Hexagon implements TurtleProgram {
    /**
     * This method contains the code of the turtle program. All TurtlePrograms must have a run method that accepts a
     * TurtleAPI (see TurtleProgram.java for more info), but its contents are up to you.
     * @param turtle the TurtleAPI object, which provides methods you can call to control the turtle.
     */
    @Override
    public void run(TurtleAPI turtle) {
        turtle.setColor(Color.RED);
        turtle.setPenDown(true);

        turtle.go(50);
        for (int i = 0; i < 5; i++) {
            turtle.turn(300);
            turtle.go(50);
        }

        turtle.setPenDown(false);
    }
}
